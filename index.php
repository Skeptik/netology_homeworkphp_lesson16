﻿<!DOCTYPE html>
<html>
<head>
	<title>Table</title>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<style type="text/css">
	   div {
	   	padding: 7px;
	    padding-right: 20px;
	    border: solid 1px black;
	    font-family: Verdana, Arial, Helvetica, sans-serif;
	    font-size: 13pt;
	   	background: #E6E6FA;
	  }
       table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }

    table th {
        background: #eee;
    }
	</style>
</head>
<body>
	<div align="center">
		<form enctype="multipart/form-data" method="POST" action="#" name="myform">
			<label for="input">Введите адрес:</label>
			<input type="text" name="input">
			<input type="submit" name="button" value="Найти">
		</form>
	</div>
    <?php
    if(!empty($_POST) && !empty($_POST['input'])){
        require_once __DIR__.'/vendor/autoload.php';
        $api = new \Yandex\Geo\Api();
        $api->setQuery($_POST['input']);
        $api
            ->setLimit(999)
            ->setLang(\Yandex\Geo\Api::LANG_RU)
            ->load();

        $response = $api->getResponse();
        $allAdres = $response->getFoundCount();
        $collection = $response->getList();
        $latitude = $collection[0]->getLatitude();
        $longitude = $collection[0]->getLongitude();
    ?>
    <table>
        <tr>
        <th>Адрес</th>
        <th>Широта</th>
        <th>Долгота</th>
        </tr>
    <?php
        foreach ($collection as $item) {
    ?>
        <tr>
            <td><?= $item->getAddress() ?></td>
            <td><?= $item->getLatitude() ?></td>
            <td><?= $item->getLongitude() ?></td>
        </tr>

    <?php
        }
    }
    ?>
    </table>
    <div id="map" style="width: 600px; height: 400px"></div>
    <script>
        addEventListener('load', inicial, false);
        function inicial()
        {
            ymaps.ready(init);
            var myMap, myPlacemark;

            function init(){
            myMap = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 7
            });
            var latitude = "<? echo $latitude ?>";
            var longitude = "<? echo $longitude ?>";
            if(latitude && longitude){
                myMap.setCenter([latitude, longitude]);
                myPlacemark = new ymaps.Placemark([latitude, longitude], {}, {
                    preset: 'islands#redIcon'
                });
                myMap.geoObjects.add(myPlacemark);
            }
        }
    }
    </script>
</body>
</html>